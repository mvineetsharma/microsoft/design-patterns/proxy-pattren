﻿using System;

// The Fruit interface
public interface IFruit
{
    void DisplayInfo();
}

// RealFruit class implementing the Fruit interface
public class RealFruit : IFruit
{
    private string name;

    public RealFruit(string name)
    {
        this.name = name;
        // Simulate some expensive initialization or loading of fruit information
        Console.WriteLine($"Loading information for {name}");
    }

    public void DisplayInfo()
    {
        Console.WriteLine($"This is a {name}");
    }
}

// FruitProxy class implementing the Fruit interface
public class FruitProxy : IFruit
{
    private RealFruit realFruit;
    private string name;

    public FruitProxy(string name)
    {
        this.name = name;
    }

    public void DisplayInfo()
    {
        if (realFruit == null)
        {
            // Create the real Fruit object only when needed
            realFruit = new RealFruit(name);
        }
        realFruit.DisplayInfo();
    }
}

class Program
{
    static void Main()
    {
        // Create a FruitProxy for an apple
        IFruit appleProxy = new FruitProxy("Apple");

        // Display information about the apple (loads the real fruit information)
        appleProxy.DisplayInfo();

        // Display information about the apple again (uses the cached real fruit)
        appleProxy.DisplayInfo();
    }
}
